<?php

class Controller {

	protected
		$framework,
		$db;

	function __construct() {
		$f3=Base::instance();
		try {
			// Connect to db
			$db=new DB\SQL(
			'mysql:host=localhost;port=3306;dbname='.$f3->get('dbname'), $f3->get('dbuser'),$f3->get('dbpasswd'));
		} catch (Exception $e) {
			die("Connection error! Please open file: <b>/config/config.ini</b> and change <code>dbname, dbpasswd, dbuser</code> strings");
		}
		$this->framework=$f3;
		$this->db=$db;
	}
}
